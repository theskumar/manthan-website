<?php	
	//site meta datas goes here
	define("site_name","Manthan",1);
	define("site_title", "Manthan 2012 - A management conclave on India Vision beyond 2020", 1);
	define("site_type", "Organisation",1);
	
	define('site_description', "Manthan 2012 is a management conclave on the topic India Vision beyond 2020 to be held on 21st and 22nd April in SMVD University.",1);
	
	define('site_keywords', "smvdu, manthan, managment conclave, 2012, event, ",1);
	define('site_photo_url', "https://fbcdn-sphotos-a.akamaihd.net/hphotos-ak-prn1/551507_144523725673904_144261562366787_171179_1005342623_n.jpg");
	define('site_author', 'Saurabh Kumar http://saurabhworld.in',1);
	
	//add links here
	define('site_twitter_url', "http://twitter.com/manthan_smvdu",1);	
	define('site_facebook_url', "https://www.facebook.com/manthan.smvdu",1);	
	
	define('site_url',"http://manthan.smvdu.net.in",1);
	define('fest_poster_url', "",1);	

	define('site_fb_appid','',1);
	define('analytics_id', '',1);
	
	
	/*
	 * settings.
	 *
	 ************************************/
	
	define('DEBUG', 0);
