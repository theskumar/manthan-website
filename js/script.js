/* Author: Saurabh Kumar
		   http://saurabhworld.in
   Url: http://dev.saurabhworld.in/manthan-website
*/



jQuery(function( $ ){
	
	// Scroll initially if there's a hash (#something) in the url 
	// $.localScroll.hash({		
	// 	queue:true,
	// 	duration:1500
	// });
	
	$.localScroll({
		queue:true,
		duration:1000,
		hash:true,
		onBefore:function( e, anchor, $target ){
			// The 'this' is the settings object, can be modified
		},
		onAfter:function( anchor, settings ){
			// The 'this' contains the scrolled element (#content)
		}
	});

	$('iframe.video').load(function () {		
		if(window.location.hash) {
			// Fragment exists
			// just display it
			$(this).fadeIn('slow');
		} else {
			// Fragment doesn't exist
			$('html, body').animate({scrollTop: $('body').offset().top+160}, 'slow');
			$(this).delay('1000').slideDown('slow');		

		}
		
		//$('body').scrollTop($('body').scrollTop() + 150);
	});

	// credits
	$('#a-credit-saurabh').hovercard({
		showTwitterCard: true
	});


});



/* Latest updates */
(function() {
	var count = 0,

		list = [
			{
				service: 'twitter',
				user: 'manthan_smvdu'
			}
		];

	Date.prototype.toISO8601 = function(date) {
		var pad = function(amount, width) {
				var padding = "";
				while (padding.length < width - 1 && amount < Math.pow(10, width - padding.length - 1))
				padding += "0";
				return padding + amount.toString();
			};
		date = date ? date : new Date();
		var offset = date.getTimezoneOffset();
		return pad(date.getFullYear(), 4) + "-" + pad(date.getMonth() + 1, 2) + "-" + pad(date.getDate(), 2) + "T" + pad(date.getHours(), 2) + ":" + pad(date.getMinutes(), 2) + ":" + pad(date.getUTCSeconds(), 2) + (offset > 0 ? "-" : "+") + pad(Math.floor(Math.abs(offset) / 60), 2) + ":" + pad(Math.abs(offset) % 60, 2);
	};

	$("#updates-list").lifestream({
		limit: 4,
		list: list,
		feedloaded: function() {
			count++;
			// Check if all the feeds have been loaded
			if (count === list.length) {
				$("#updates-list li").each(function() {
					var element = $(this),
						date = new Date(element.data("time"));
					element.append(' <abbr class="timeago" title="' + date.toISO8601(date) + '">' + date + "</abbr>");
				});
				$("#updates-list .timeago").timeago().css('font-size', 'smaller');
			}
		}
	});

})();