<!doctype html>
<!--
  /**
  *
  * Manthan official website
  * http://manthan.smvdu.net.in
  *
  * Author: Saurabh Kumar
  *         http://saurabhworld.in
  *         Twitter: @saurabh_world
  *
  * License: Under Creative Common Licence
  *          I don't care for attributions.
  *
  * Accessed on: <?php echo date('M d, Y (h:ia)'); ?>
  */
-->
<?php include ('php/functions.php');  $production = true; ?>
<html itemscope itemtype="http://schema.org/<?php echo SITE_TYPE; ?>" class=no-js>
<head>
  <meta charset="utf-8">	
	
  <title><?php echo site_title; ?></title>
		
	
  <meta name="viewport" content="width=device-width,initial-scale=1">	
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	

  <meta itemprop="name" content="<?php print(site_title)?>">
  <meta name="description" content="<?php print(site_description);?>">
  <meta itemprop="description" content="<?php print(site_description);?>">
  <meta itemprop="image" content="<?php print(site_photo_url);?>">
  <meta name="keywords" content="<?php print(site_keywords);?>">
  <meta name="author" content="<?php print(site_author);?>">
	
  <?php get_fb_meta(); ?>
	
  <?php // include site css here ?>	
  <link rel="stylesheet" href="css/style.css">

  <link rel="stylesheet/less" href="css/less/bootstrap.less">
  <script src="js/libs/less.js"></script> */ ?>


	  
  <?php // More ideas for your <head> here: h5bp.com/d/head-Tips ?>

  <?php // All JavaScript at the bottom, except this Modernizr build incl. Respond.js
       // Respond is a polyfill for min/max-width media queries. Modernizr enables HTML5 elements & feature detects; 
      // for optimal performance, create your own custom Modernizr build: www.modernizr.com/download/ ?>
  <script src="js/libs/modernizr-2.5.3.min.js"></script>
</head>

<body data-spy="scroll">

  <img src="img/body-bg-fullscreen.jpg" alt="" id="fullscreen-bg-fixed">  
  
  <header>
    <div id="logo-top-main" class="ir">
      <h1>Manthan 2012</h1>
      <p>A managment Conclave</p>
    </div>
    <div class="top-right">      
      <p class="info">Connect with us</p>
      <ul class="social-links">
        
        <!-- <li><a href="#" class="facebook" target="_blank" title="Subscribe via RSS Feed"><img src="img/rss-feed.png" alt="RSS Feed"></a></li> -->

        <li><a href="<?php echo site_twitter_url; ?>" target="_blank" class="twitter" title="Follow us on Twitter"><img src="img/twitter.png" alt="Twitter"></a></li>
        
        <li><a href="<?php echo  site_facebook_url; ?>" class="facebook" target="_blank"title="Connect on Facebook"><img src="img/facebook.png" alt="Facebook"></a></li>

      </ul>
      <div class="like-button">
        
      </div>      
    </div>
  </header>
 
  <aside class="fixed">
    <nav>
      <ul class="nav nav-main">
        <li><a href="#home">home</a></li>
        <li><a href="#speakers">speakers</a></li>
        <li><a href="#schedule">schedule</a></li>
        <li><a href="#about">about</a></li>
        <li><a href="#registration">register</a></li>       
        <li><a href="#contact">contact</a></li>
      </ul>          
    </nav>
  </aside>
  

  <div id="wrapper">      
    
    <section id="home">
      
      <article class="banner">
        <h1 class="title"><strong>Manthan:</strong> A Management Conclave</h1>
        <h2>on</h2>
        <h2>"India: Vision Beyond 2020"</h2>
        <p class="date">April 21<sup>st</sup> - 22<sup>nd</sup>, 2012</p>
      </article>
      <div class="promo">
        <iframe class="video" src="http://player.vimeo.com/video/40636439?title=0&amp;byline=0&amp;portrait=0&amp;color=ff0179&amp;autoplay=1" width="100%" height="500px" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
      </div>
      
      <div class="row-fluid more-info">
        <article class="span4">
          <h3>Organised by:</h3>
          <p>
            <strong>Shri Mata Vaishno Devi University</strong> <br>
            Katra, Jammu &amp; Kashmir - 182320 <br>
            Website: <a href="http://smvdu.net.in">http://smvdu.net.in</a> <br>            
          </p>

          <p><strong>Venue:</strong> Matrika Auditorium</p>

          <p>
            <a class="btn btn-success" href="http://smvdu.net.in/about/location.html">View Map &raquo;</a>
          </p>
        </article>
        
        <article class="span8">
          <h3>Updates:</h3>
          
          <p id="updates-list"> Loading... </p>
          <p>Get all updates via <a href="http://twitter.com/manthan_smvdu">twitter</a></p>
          

        </article>        
      </article>
        
    </section>

    <section id="sponsors">
      
      <h3>Sponsored by:</h3>
      
      <p class="visuallyhidden">The manthan event would not have been possible without the kind gesture and support from our sponsors. We are really thankful to them helping us make this event a success.</p>

        <ul class="thumbnails sponsors clearfix">
          <?php get_sponsors_list(); ?>
        </ul>
      
      <p class="visuallyhidden">Again, if you would like to get involved and be a part of this event, we would very happy to attend you. Please do <a href="#contacts">contact</a> us!!</p>
    
    </section>



    
    <section id="speakers">
      
      <h1>Speakers</h1>

      <p class="visuallyhidden">Following are the list of speakers for the two the event Manthan at Shri Mata Vaishno Devi University. </p>

      <div class="row clearfix">
          <?php get_speaker_list(); ?>
      </div>

    </section>
    




    <section id="schedule">
      
      <h1>Schedule</h1>
      
      <p>
        Manthan is a two day event starting from 21st April 2010 that 
        includes lectures, panel discussions, cultural events and 
        paper-presentations.</p>
      
      <?php get_schedule(); ?>
      
      
    </section>



    <section id="about">       
      <h1>About</h1>       

      <p>The focus of Management Conclave is to get Corporate Professionals, Academicians, Research Scholars and Management Students together on single platform for a thorough discussion on <strong>‘INDIA: VISION BEYOND 2020’</strong>. The conclave tends to cultivate various issues of changing business environment in India. Deliberating on policy and regulatory environment that poses opportunities and challenges and understanding the managerial challenges faced by different industries in various management disciplines. Also, to forecast the future trends in different industries globally and in India.</p>

      <h3>Scope and Objective</h3>

      <p>
        Management challenges together with increasing expectations of various stakeholders pose several issues, which must be discussed in a right forum. Therefore, we intend to bring dynamic personalities to deliberate on those managerial issues of the current days in the context of India in particular. 
      </p>

      <p>
        The objectives of the management conclave can thus be outlined as below:
        <ul>
          <li>Deliberating on policy and regulatory environment that poses opportunities and challenges.</li> 
          <li>Understanding the managerial challenges faced by different industries in various management disciplines like project management, operations, distribution, marketing, financing, pricing etc. especially in the light of changing competition, environmental awareness and future competing products.</li>
          <li>To forecast the future trends in different industries globally and in India.</li>
        </ul>
      </p> 


        
        <h3>Target Audience:</h3>
        <ul>
          <li>Management students of various universities, colleges and institutes in India.</li>
          <li>Faculties from different management institutes and colleges.</li>
          <li>Academicians and Researchers from different institutes and colleges.</li>
          <li>Final year graduation students.</li> 
          <li>Various people from different industries.</li>
          <li>Non Government Organisations.</li>
        </ul>

      

    </section><!-- about section end -->


    <section id="registration">
      <h1>Registration Details</h1>
      
      <div class="row-fluid">        
        
        <article class="span6">
          <h2>For Students<sup>*</sup></h2>
          <p><span class="fee">Rs. 500/- </span> (upto 5 students)</p>
          <p><span class="fee">Rs. 450/- </span> (upto 10 students)</p>
          <p><span class="fee">Rs. 400/- </span> (above 10 students)</p>

          <p><sup>*</sup>inlcudes: fooding and lodging</p>
        </article>
        
        <article class="span6">
          <h2>For Corporate delegate</h2>
          <p><span class="fee">Rs. 750/-</span> per participant per day</p>
          <!-- <p>inlcudes: Executive folder, Note pad, Pen</p> -->
        </article>
      </div>
    </section>



    <section id="contact">
      <h1 class="heading">Contacts</h1>
      
      <div class="row">
        <div class="span4">
          <h3>Conclave Coordinator</h3>
          <p>
              Dr. Hari Govind Mishra <br>
              Management Conclave Co-ordinator<br>
              EPABX:  01991-285524 extn: 2412<br>
              Mobile:+919419165846<br>
              Fax: 01991-285694<br>
          </p>
        </div>
        <div class="span4">
          <h3>Student Coordinator</h3>
          <p>
              Surabhi Koul<br>
              managementconclave2012.smvdu@gmail.com
          </p>
        </div>

        <div class="span4">
          <h3>Organizing Secretary</h3>
          <p>
            Ankush Verma (+919419190031) <br>
            Aditya Chuni (+919419118682) <br>
          </p>
        </div>  

        <div class="span4">
          <h3>Event Cordinator</h3>
          <p>
           Sahil Sharma (+919796493636) <br>
           Angad Singh Sodhi (+919797300003) <br>
          </p>
        </div>

        <div class="span4">
          <h3>Core Committee</h3>
          <p>
              Tarun Sharma (+919419730374) <br>
              Sahil Bhagotra (+919419202645) <br>
              Sudhir Manhas (+919419103497) <br>
              Anuj Sharma (+919796690152) <br>
              Akhil Verma (+919796206397) <br>
          </p>
        </div>


      </div>

      <div class="row">
          <div class="span12">
             <!-- <p><strong>Email:</strong>managementconclave2012.smvdu@gmail.com</p> -->
            <p>Twitter: <a href="http://twitter.com/manthan_smvdu" target="_blank">http://twitter.com/manthan_smvdu</a></p>
            <p>Facebook: <a href="https://www.facebook.com/manthan.smvdu" target="_blank">Visit the page</a></p> 
          </div>
      </div>

      <footer class="row" id="site-footer">        
        <div class="pull-left">
          Copyright &copy; <?php echo date('Y');?> <a href="http://smvdu.net.in">SMVD University</a>
        </div>
        <div class="pull-right">
          <a href="#home">Home</a> |
          <a href="#sponsors">Sponsors</a> |
          <a href="#about">About</a> | 
          <a href="http://smvdu.net.in/disclaimer.html">Disclaimer</a> |
          <a href="mailto:managementconclave2012.smvdu@gmail.com">Contact</a>
        </div>
      </footer>

      
    </section>

  </div> <?php //wrapper for all the the contents, only thing outside this is fullscreen image ?>

    <footer id="fixed-left">
      <div class="icons"></div>
      <div class="credit">
        Website designed by 
        <a href="http://saurabhworld.in" 
          title="Saurabh Kumar"
          data-hovercard="saurabh_world"
          id="a-credit-saurabh"
          >Saurabh Kumar</a>
      </div>        
    </footer>

  <!-- JavaScript at the bottom for fast page loading -->
   <?php // Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline ?>
  <?php if($production){ ?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <?php } ?>  
  <script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.1.min.js"><\/script>')</script> 


  <?php //scripts to be concatenated and minified ?>
      <script defer src="js/plugins.js"></script>
      <script defer src="js/script.js"></script>

  <?php //end scripts ?>

  <?php //Asynchronous Google Analytics snippet. Change UA-XXXXX-X to be your site's ID. ?>
  <script>
    var _gaq=[['_setAccount','UA-25780349-3'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
  </script>

</body>
</html>

